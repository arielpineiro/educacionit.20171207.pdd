﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAOs
{
    class Program
    {
        static void Main(string[] args)
        {
            BaseDao baseDao = new CustemerDao();
            baseDao.Execute();
        }
    }

    abstract class BaseDao
    {    
        /*
         * Plantilla, Template Method
         * */
        public void Execute()
        {
            OpenConnection();
            GetQuery();
            CloseConnection();
        }

        public void OpenConnection()
        {
            Console.WriteLine("Opening connection...");
        }
        public void CloseConnection()
        {
            Console.WriteLine("Closeing connection...");
        }
        public abstract void GetQuery();
    }

    class ProductDao : BaseDao
    {
        public override void GetQuery()
        {
            Console.WriteLine("SELECT * FROM Products");
        }
    }

    class CustemerDao : BaseDao
    {    
        public override void GetQuery()
        {
            Console.WriteLine("SELECT * FROM Customers");
        }        
    }
}
