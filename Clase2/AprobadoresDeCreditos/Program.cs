﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AprobadoresDeCreditos
{
    class Program
    {
        /// <summary>
        /// Mains the specified arguments.
        /// </summary>
        /// <param name="args">The arguments.</param>
        static void Main(string[] args)
        {
            double mount = 650000;

            Approver employee = new Employee();
            Approver supervisor = new Supervisor();
            Approver manager = new Manager();

            employee.SetNext(supervisor);
            supervisor.SetNext(manager);

            employee.HandleRequest(mount);
        }
    }

    abstract class Approver
    {
        protected Approver next;
        public abstract void HandleRequest(double mount);

        public void SetNext(Approver approver)
        {
            this.next = approver;
        }
    }

    class Employee : Approver
    {
        public override void HandleRequest(double mount)
        {
            if (mount < 50000)
            {
                Console.WriteLine("Approve it" + this.GetType().Name);
            }
            else
            {
                this.next.HandleRequest(mount);
            }
        }
    }

    class Supervisor : Approver
    {
        public override void HandleRequest(double mount)
        {
            if ((mount > 50000) && (mount < 500000))
            {
                Console.WriteLine("Approve it" + this.GetType().Name);
            }
            else
            {
                this.next.HandleRequest(mount);
            }
        }
    }

    class Manager : Approver
    {
        public override void HandleRequest(double mount)
        {
            if ((mount > 500000) && (mount < 1000000))
            {
                Console.WriteLine("Approve it" + this.GetType().Name);
            }
            else
            {
                throw new NotImplementedException();
            }
        }
    }
}
