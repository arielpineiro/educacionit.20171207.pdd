﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facilitador
{
    class Program
    {
        static void Main(string[] args)
        {
            // Facade Pattern
            Approver approver = new Approver();
            approver.Request(123123);
        }
    }

    class Approver
    {
        Device device;
        MyFramework myFramework;
        ThirdParty thirdParty;
        WebService webService;

        public bool Request(double mount)
        {
            device.Foo();
            myFramework.Foo();
            thirdParty.Foo();
            webService.Foo();

            return true;
        }
    }


    class Device
    {
        public void Foo()
        {

        }
    }

    class WebService
    {
        public void Foo()
        {

        }
    }

    class ThirdParty
    {
        public void Foo()
        {

        }
    }

    class MyFramework
    {
        public void Foo()
        {

        }
    }
}
