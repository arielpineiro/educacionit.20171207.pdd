﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiCalculadora
{
    class Program
    {
        static void Main(string[] args)
        {
            ICalculadora calculadora = new IntermediarioCalculadora();
            double resultado = calculadora.Sumar(2, 3);
        }
    }

    interface ICalculadora
    {
        double Dividir(double a, double b);
        double Multiplicar(double a, double b);
        double Restar(double a, double b);
        double Sumar(double a, double b);
    }

    class IntermediarioCalculadora : ICalculadora
    {
        Calculadora calculadora = new Calculadora();

        public double Dividir(double a, double b)
        {
            throw new NotImplementedException();
        }

        public double Multiplicar(double a, double b)
        {
            throw new NotImplementedException();
        }

        public double Restar(double a, double b)
        {
            throw new NotImplementedException();
        }

        public double Sumar(double a, double b)
        {
            Console.WriteLine("Registrando operación...");
            return calculadora.Sumar(a, b);
        }
    }

    class Calculadora : ICalculadora
    {
        public double Sumar(double a, double b)
        {
            return a + b;
        }
        public double Restar(double a, double b)
        {
            return a - b;
        }
        public double Multiplicar(double a, double b)
        {
            return a * b;
        }
        public double Dividir(double a, double b)
        {
            return a / b;
        }
    }
}
