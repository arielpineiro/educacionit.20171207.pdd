﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iteraciones
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> values = new List<int> { 2, 3, 4, 5, 6 };
            
            foreach (var item in values)
            {
                Console.WriteLine();
            }
        }
    }
}
