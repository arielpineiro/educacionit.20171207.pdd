﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers
{
    class Program
    {
        static void Main(string[] args)
        {
            Customer customer = Customer
                .Create()           
                .SetAge(22)
                .SetFirstName("Jorge")
                .SetLastName("Sanchez")
                .SetPhoneNumber("11223344")
                .Build();            
        }
    }

    class Customer
    {
        int id;
        string firstName;
        string lastName;
        int age;
        string phoneNumber;
        static Customer customer;
        bool genre;

        public static Customer CreateFemale()
        {
            customer = new Customer();           
            return customer;
        }

        public Customer SetFirstName(string firstName)
        {
            this.firstName = firstName;
            return customer;
        }

        public Customer SetLastName(string lastName)
        {
            this.lastName = lastName;
            return customer;
        }

        public Customer SetAge(int age)
        {
            this.age = age;
            return customer;
        }

        public Customer SetPhoneNumber(string phoneNumber)
        {
            this.phoneNumber = phoneNumber;
            return customer;
        }

        public Customer Build()
        {
            return customer;
        }
    }
}
