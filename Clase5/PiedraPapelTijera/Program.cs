﻿using System;
using static System.Console;

namespace PiedraPapelTijera
{
    class Program
    {
        static void Main(string[] args)
        {
            Hand hand1 = new Paper();
            Hand hand2 = new Scissors();
           
            bool result = hand1.loseAgainst(hand2);
            WriteLine(result);
        }
    }

    abstract class Hand
    {
        public abstract bool loseAgainst(Hand hand);
        public abstract bool loseAgainst(Paper paper);

        public abstract bool loseAgainst(Scissors scissors);

        public abstract bool loseAgainst(Rock rock);

    }

    class Paper : Hand
    {
        public override bool loseAgainst(Scissors scissors)
        {
            return true;
        }

        public override bool loseAgainst(Paper paper)
        {
            return false;
        }

        public override bool loseAgainst(Rock rock)
        {
            return false;
        }

        public override bool loseAgainst(Hand hand)
        {
            return hand.loseAgainst(this);
        }
    }

    class Scissors : Hand
    {
        public override bool loseAgainst(Paper paper)
        {
            return false;
        }

        public override bool loseAgainst(Scissors scissors)
        {
            return false;
        }

        public override bool loseAgainst(Rock rock)
        {
            return true;
        }

        public override bool loseAgainst(Hand hand)
        {
            return hand.loseAgainst(this);
        }
    }

    class Rock : Hand
    {
        public override bool loseAgainst(Paper paper)
        {
            throw new NotImplementedException();
        }

        public override bool loseAgainst(Scissors scissors)
        {
            throw new NotImplementedException();
        }

        public override bool loseAgainst(Rock rock)
        {
            throw new NotImplementedException();
        }

        public override bool loseAgainst(Hand hand)
        {
            return hand.loseAgainst(this);
        }
    }
}
