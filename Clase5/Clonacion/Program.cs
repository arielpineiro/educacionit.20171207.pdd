﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clonacion
{
    class Program
    {
        static void Main(string[] args)
        {
            Customer c1 = new Customer { Name = "Jorge Sanchez" };
            Customer c2 = c1.ShallowCopy();

            c2.Name = "María de los Angeles";
        }
    }

    class Customer
    {
        public string Name { get; set; }

        public Customer ShallowCopy()
        {
            return (Customer)this.MemberwiseClone();
        }

        public Customer DeepCopy()
        {
            Customer clone = (Customer)this.MemberwiseClone();
            clone.Name = string.Copy(Name);
            return clone;
        }
    }
}
