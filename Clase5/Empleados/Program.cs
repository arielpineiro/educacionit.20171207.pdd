﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Empleados
{
    class Program
    {
        static void Main(string[] args)
        {
            Rule rule = new Vacation();

            Employee director = new Director("Jorge Sanchez", 150000, 20);

            director.Accept(rule);
        }
    }

    abstract class Rule
    {
        public abstract void Apply(Employee employee);
        public abstract void Apply(Director director);
        public abstract void Apply(Manager manager);
    }

    class Vacation : Rule
    {
        public override void Apply(Director director)
        {
            Console.WriteLine("Incrementar para un director los dias de vacaciones...");
        }

        public override void Apply(Manager manager)
        {
            Console.WriteLine("Incrementar para un manager los dias de vacaciones...");
        }

        public override void Apply(Employee employee)
        {
            employee.Accept(this);
        }
    }

    class Salary : Rule
    {
        public override void Apply(Director director)
        {
            throw new NotImplementedException();
        }

        public override void Apply(Manager manager)
        {
            throw new NotImplementedException();
        }

        public override void Apply(Employee employee)
        {
            throw new NotImplementedException();
        }
    }

    abstract class Employee
    {
        string name;
        double salary;
        int vacationDays;

        public Employee(string name, double salary, int vacationDays)
        {
            this.name = name;
            this.salary = salary;
            this.vacationDays = vacationDays;
        }

        public abstract void Accept(Rule rule);
        public abstract void Accept(Vacation rule);
        public abstract void Accept(Salary rule);
        }

    class Director : Employee
    {
        public Director(string name, double salary, int vacationDays) : base(name, salary, vacationDays)
        {
        }

        public override void Accept(Rule rule)
        {
            rule.Apply(this);
        }

        public override void Accept(Vacation rule)
        {
            throw new NotImplementedException();
        }

        public override void Accept(Salary rule)
        {
            throw new NotImplementedException();
        }
    }

    class Manager : Employee
    {
        public Manager(string name, double salary, int vacationDays) : base(name, salary, vacationDays)
        {
        }

        public override void Accept(Rule rule)
        {
            throw new NotImplementedException();
        }

        public override void Accept(Vacation rule)
        {
            throw new NotImplementedException();
        }

        public override void Accept(Salary rule)
        {
            throw new NotImplementedException();
        }
    }
}
