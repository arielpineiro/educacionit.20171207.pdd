﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComponentesVisuales
{
    class Program
    {
        static void Main(string[] args)
        {
            CompositeElement root = new CompositeElement("mainForm");
            CompositeElement menu = new CompositeElement("menu");
            CompositeElement toolbar = new CompositeElement("toolbar");

            SimpleElement visualStudioHomePage = new SimpleElement("visualStudioHomePage");

            root.Add(menu);
            root.Add(toolbar);

            toolbar.Add(visualStudioHomePage);

            

            root.Show();
        }
    }

    abstract class DrawingElement
    {
        protected string name;
        public DrawingElement(string name)
        {
            this.name = name;
        }

        public abstract void Add(DrawingElement drawingElement);
        public abstract void Show();
    }

    class SimpleElement : DrawingElement
    {
        public SimpleElement(string name) : base(name)
        {
        }

        public override void Add(DrawingElement drawingElement)
        {
            throw new NotImplementedException();
        }

        public override void Show()
        {
            Console.WriteLine("Show simple element: " + name);
        }
    }

    class CompositeElement : DrawingElement
    {
        List<DrawingElement> elements = new List<DrawingElement>();

        public CompositeElement(string name) : base(name)
        {
        }

        public override void Add(DrawingElement drawingElement)
        {
            this.elements.Add(drawingElement);
        }

        public override void Show()
        {
            Console.WriteLine("Showing complex element: " + name);
            foreach (DrawingElement element in elements)
            {
                element.Show();
            }
        }       
    }
}
