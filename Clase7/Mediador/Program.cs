﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediador
{
    class Program
    {
        static void Main(string[] args)
        {
            ChatRoom chatRoom = ChatRoom.GetInstance();

            Participant Juan = new Participant("Juan", new ConsoleDevice());
            Participant Maria = new Participant("Maria", new BrowserDevice());
            Participant Bob = new Participant("Bob", new BrowserDevice());

            try
            {
                chatRoom.Join(Juan);
                chatRoom.Join(Maria);
                chatRoom.Join(Bob);              
            }
            catch (ParticipantsExceededException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (ParticipantAlreadyExistsException e)
            {
                Console.WriteLine(e.Message);
            }

            try
            {
                Juan.Send(new Unicast("¿Cómo estás María?", Maria));
                Maria.Send(new Broadcast("Hola! Soy nuevá aquí, ¿cómo están todos?"));
            }
            catch (ParticipantNotExistsException e)
            {
                Console.WriteLine(e); ;
            }
        }
    }

    class ChatRoom
    {
        private IDictionary<string, Participant> participants = new Dictionary<string, Participant>();
        private static readonly ChatRoom instance = new ChatRoom();
        private static object padlock = new object();

        private ChatRoom()
        {
            
        }

        public static ChatRoom GetInstance()
        {
            return instance;
        }

        public void Join(Participant participant)
        {
            
            if (!participants.ContainsKey(participant.GetNickName()))
            {
                if (participants.Count() == 2)
                    throw new ParticipantsExceededException();

                participants.Add(participant.GetNickName(), participant);
            }
            else
            {
                throw new ParticipantAlreadyExistsException();
            }

            participant.SetChatRoom(this);
        }

        public void Send(Participant from, Unicast message)
        {
            if (participants.ContainsKey(from.GetNickName()))
            {
                message.SetFrom(from);

                Participant to = participants[message.GetTo().GetNickName()];

                to.Receive(message);
            }
            else
            {
                throw new ParticipantNotExistsException();
            }
        }

        public void Send(Participant from, Broadcast message)
        {
            foreach (KeyValuePair<string, Participant> pair in participants)
            {
                message.SetFrom(from);
                pair.Value.Receive(message);
            }
        }
    }

    interface IReceiver
    {
        void Receive(Unicast message);
        void Receive(Broadcast message);
    }

    class Participant : IReceiver
    {
        private string nickname;
        private ChatRoom chatRoom;
        private IDevice device;

        public Participant(string nickname)
        {
            this.nickname = nickname;
        }

        public Participant(string nickname, IDevice device) : this(nickname)
        {
            this.device = device;
        }

        public string GetNickName()
        {
            return this.nickname;
        }

        public IDevice GetDevice()
        {
            return this.device;
        }

        public void SetChatRoom(ChatRoom chatRoom)
        {
            this.chatRoom = chatRoom;
        }
        
        public void Send(Unicast message)
        {
            chatRoom.Send(this, message);
        }

        public void Send(Broadcast message)
        {
            chatRoom.Send(this, message);
        }

        public void Receive(Unicast message)
        {
            if (HasDevice() && message.GetTo() == this)
            {
                Console.WriteLine(GetNickName() + ": Recibiendo mensaje directo de: " + message.GetFrom().GetNickName());
                this.device.Show(message.GetText());
            }
        }

        public void Receive(Broadcast message)
        {
            if (HasDevice()  && message.GetFrom() != this)
            {
                Console.WriteLine("Recibiendo mensaje de difusión de: " + message.GetFrom().GetNickName());
                this.device.Show(message.GetText());
            }
        }

        public bool HasDevice()
        {
            return this.device != null;
        }
    }

    class Message
    {
        protected string text;
        protected Participant from;

        public Message(string text)
        {
            this.text = text;
        }

        public Participant GetFrom()
        {
            return this.from;
        }

        public void SetFrom(Participant from)
        {
            this.from = from;
        }

        public string GetText()
        {
            return this.text;
        }
    }

    class Unicast : Message
    {
        Participant to;

        public Unicast(string text) : base(text)
        {
        }

        public Unicast(string text, Participant to) : this(text)
        {
            this.to = to;
        }

        public Participant GetTo()
        {
            return this.to;
        }
    }

    class Broadcast : Message
    {
        public Broadcast(string text) : base(text)
        {
        }
    }

    class Multicast : Message
    {
        public Multicast(string text) : base(text)
        {
        }
    }

    interface IDevice
    {
        void Show(string input);
    }

    class ConsoleDevice : IDevice
    {
        public void Show(string input)
        {
            Console.WriteLine("Console: " + input);
        }
    }

    class BrowserDevice : IDevice
    {
        public void Show(string input)
        {
            Console.WriteLine("Chrome: <html>" + input + "</html>");
        }
    }


    class ParticipantAlreadyExistsException : Exception
    {

    }

    class ParticipantNotExistsException : Exception
    {

    }

    class ParticipantsExceededException : Exception
    {

    }
}
