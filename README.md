## Patrones de Diseño

Problema Clase 1

Tengo una jerarquía de empleados, y quiero poder tener una función
que dependiendo del tipo calcule el salario de dicho empleado.

La jerarquía de empleados es: Gerente, Secretaria, Supervisor, Vendedor y Vendedor por comisión

5000 + (10 * cantidadDeVentas)

Problema 1
	Tenemos un balanceador de carga y debemos resolver el problema
	de tener una única instancia.
	
	Solución: Patron Singleton

Problema 2
	Tenemos una clase Lista con dos métodos posibles de ordenamiento
	
	Solución: Patron Strategy
	
Problema 3
	Tenemos que crear un tipo de dato abstracto, que me permita de manera unívoca, ejecutar una consulta a la base de datos. Y diferentes tipos de datos que deriven del mismo con la responsabilidad de entender / conocer la consulta especifica a la base de datos.
	
	DAO (Data Access Object) 
	
	Solución: Template method
	
Clase 2
	
Problema 4
	
	Quiero crear una calculadora. Con las operaciones basicas, y poder hacer uso de un intermediario para agregar comportamiento. Sin romper la interfaz.
	
	Solución: Patrón Proxy
	
Problema 5

	Quiero crear una sintaxis sencilla para poder crear objetos. Y hacer más facil la configuración y creación del mismo.
	
	Solución: Patrón Builder
	
Problema 6

	Tenemos diferentes aprobadores que aprueban creditos dependiendo del monto, si el aprobador no puede aprobar (por el monto) tiene que ceder esa responsabilidad al siguiente.
	
	0 < x < 50000 Empleado
	50000 < x < 500000 Supervisor
	500000 < x < 1000000 Gerente
	x > 1000000 Comisión directiva
	
	Solución: Chain of Responsibility
	
Problema 7

	Tengo diferentes componentes (pueden ser mios o no), por lo general complejos y necesito de ellos para resolver un problema especifico. 
	
	Solución: Facade Pattern
	

Problema 8

	Iterator pattern, mecanismo para garatinzar el uso del foreach, o mejor dicho iterar colecciones.
	
	
Clase 3

Problema 9

	Tengo que representar tareas a partir de cierta ejecución de métodos las cuales se conocen como operaciones. La particularidad de estas tareas (que a priori pueden ser dos o mas). Además estas tareas deben ser independientes del sistema operativo a ejecutar que pueden ser dos (Windows o Linux). Windows: MSDOS, Linux: Bash
	
	Solución: Command Pattern
	
Problema 10

	Tengo dos redes sociales, Instagram y Facebook. Y tengo que crear una implemtanción que me permita postear videos o fotos dependiento de la plataforma de origen (Android | iOS).
	
	Solución: Bridge Pattern
	
Problema 11
	
	Reino Animal (PENDIENTE!!!)
	
	Solución: Abstract Factory
	
Problema 12

	Queremos crear una forma sencilla y elegante diferentes tipos de Documentos, que pueden ser Reporte o un Curriculum (Resume). Cada documento tienen diferentes páginas, que pueden ser Skills, Resumen, Experiencia Laboral, Cabecera, Educacion, etc.

	Solución: Factory Method

Problema 13

	Tenemos una interfaz grafica que dibuja Ventanas, las ventanas, pueden tener scrolls, botones de ayuda, boton de cerrar etc, y mas controles graficos.
	
	Solución: Decorator ...
	(recordar annotations y attributes). Añadir funcionalidad dinamicamente sin depender tanto de la rigidez de la herencia y sin romper la interfaz (vender, dibujar, obtener, tomar un cafe...).

Problema 14

	Tenemos que construir una sala de chat, la cual tiene participantes, cada participante se identifica con un nickname. Las operaciones basicas pueden ser registrar a la sala de chat y la otra sería poder enviar mensajes entre participantes.
	
	Solución: Mediator
	
Problema 15

	Tenemos que construir un mecanismo que me permita recuperar un estado anterior de un objeto determinado.
	
	Solución: Memento
	
Problema 16

	Nos interesa conocer los cambios de precio de los titulos (stock) e informar a los interesados pertinemente.
	
	Solución: Observer Pattern
	
Problema 17
		
	Adaptar una respuesta a otra a traves de un adaptador
	
	Solución: Adapter (Wrapper) Pattern

Problema 18

	Tenemos que crear un mecanismo sencillo para creación de objetos cuando la creación de los mismos es cara.
	
	Solución: Flyweight Pattern
	
Problema 19

	Resolver el problema de un piedra papel o tijera, a la jugada se le llama mano.
	
		Piedra > Papel = Gana Papel
		Papel > Tijera = Gana Tijera
		Tijera > Piedra = Gana Piedra
		
	Solución: Double Dispatch (no es una patrón de GoF)
	
Problema 20
	
	Dada una lista de empleados, de determindo tipo. Queremos implementar logica de negocio, que permita incrementar los dias de vacaciones y el salario.
	
	Solución: Visitor
	
Problema 21
	
	Quiero dibujar una interfaz de controles, donde los controles pueden ser compuestos y simples. Cada elemento compuesto, a su vez, puede tener cualquier tipo de elemento.
	
	Solución: Composite

Problema a resolver (tarea).
	Dado 2 tipos de naves espaciales (pequeña o grande) y 2 tipos de Asteroides (normal y explosivo) definir 4 reglas. Para conocer si un asteroide puede destruir alguna de estos tipos de naves.

		
Problema 22
	
	Queremos clonar objetos. Y existen dos formas: la clonacion profunda, y la clonacion superficial. Deep Copy / Shallow copy
	
	Solución: Prototype
		
Problema 23

	Dada una cuenta bancaria, queremos representar diferentes estados a partir del balance de la misma. En función de dicho balance, los estados pueden o no cambiar.
	
	Solución: State


Problema 24

	Queremos que dado un lenguaje y una gramatica definida como input se transforme en un output que termina represando algo entendible por el cliente.

	Solución: Interpreter pattern.

Clase 7

	* TDD (Test Driven Development) (Moq, Autofac)
	* Patrones de Arquitectura (MVC, MVVM, MVP... etc)
	* Inyeccion de Dependencias / Inversión de Control (Spring, Juice, Autofac)
	* Anti patrones
	




	
		
	
	
	

	
	