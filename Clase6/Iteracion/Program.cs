﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iteracion
{
    class Program
    {
        static void Main(string[] args)
        {
            MyCollection values = new MyCollection();
            values.Add(1);
            values.Add(10);
            values.Add(1123);
            values.Add(11);
            values.Add(3);

            Iterator iterator = values.CreateIterator();

            for (object value = iterator.First(); !iterator.IsDone(); value = iterator.Next())
            {
                Console.WriteLine(value);
            }

            MyCustomCollection myCustomCollection = new MyCustomCollection();

            foreach (var item in myCustomCollection)
            {

            }
            
        }
    }

    class MyCustomCollection : IEnumerable
    {
        public IEnumerator GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }

    class MyCollection : ICollection
    {
        ArrayList items = new ArrayList();

        public void Add(object item)
        {
            this.items.Add(item);
        }

        public Iterator CreateIterator()
        {
            return new Iterator(this);
        }

        public object Get(int index)
        {
            return items[index];
        }

        public int Count()
        {
            return items.Count;
        }
    }
    
    interface ICollection
    {
        Iterator CreateIterator();
    }

    interface IIterator
    {
        object First();
        object Next();    
        bool IsDone();
        object CurrentItem();
    }

    class Iterator : IIterator
    {
        private MyCollection myCollection;
        int current = 0;

        public Iterator(MyCollection myCollection)
        {
            this.myCollection = myCollection;
        }
        public object CurrentItem()
        {
            return myCollection.Get(current);
        }

        public object First()
        {
            current = 0;
            return myCollection.Get(current);
        }

        public bool IsDone()
        {
            return current >= myCollection.Count();
        }

        public object Next()
        {
            current = current + 1;
            if (!IsDone())
            {
                return myCollection.Get(current);
            }
            else
            {
                return null;
            }
        }
    }
}
