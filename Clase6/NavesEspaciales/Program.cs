﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NavesEspaciales
{
    class Program
    {
        static void Main(string[] args)
        {
            SpaceShip spaceShip = new SpaceShip();
            SpaceShip giantSpaceShip = new GiantSpaceShip();
            Asteroid asteroid = new Asteroid();
            Asteroid explodingAsteroid = new ExplodingAsteroid();

            Collider.Shoot(spaceShip, asteroid);
            Collider.Shoot(spaceShip, explodingAsteroid);
            Collider.Shoot(giantSpaceShip, asteroid);
            Collider.Shoot(giantSpaceShip, explodingAsteroid);
        }
    }

    class Collider
    {
        public static void Shoot(SpaceShip spaceShip, Asteroid asteroid)
        {
            spaceShip.collideWith(asteroid);
        }
    }

    class SpaceShip
    {
        public virtual void collideWith(Asteroid asteroid)
        {
            asteroid.collideWith(this);
        }
    }

    class Asteroid
    {
        public virtual void collideWith(SpaceShip spaceShip)
        {
            Console.WriteLine("El Asteroide colisiona con Nave Espacial");
        }

        public virtual void collideWith(GiantSpaceShip giantSpaceShip)
        {
            Console.WriteLine("El Asteroide colisiona con Nave Espacial Gigante");
        }
    }

    class GiantSpaceShip : SpaceShip
    {
        public override void collideWith(Asteroid asteroid)
        {
            asteroid.collideWith(this);
        }
    }
    class ExplodingAsteroid : Asteroid
    {
        public override void collideWith(SpaceShip spaceShip)
        {
            Console.WriteLine("El Asteroide Explisivo colisiona con Nave Espacial");
        }

        public override void collideWith(GiantSpaceShip giantSpaceShip)
        {
            Console.WriteLine("El Asteroide Explosivo colisiona con Nave Espacial Gigante");
        }
    }
}
