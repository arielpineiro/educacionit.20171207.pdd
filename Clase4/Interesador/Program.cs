﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interesador
{
    class Program
    {
        static void Main(string[] args)
        {
            Stock meli = new Stock("MELI", 120.00);            
            meli.Attach(new Investor("Juan"));
            meli.Attach(new Investor("Maria"));

            meli.SetPrice(120.00);
            meli.SetPrice(121.00);
            meli.SetPrice(118.00);
            meli.SetPrice(132.00);
            meli.SetPrice(160.00);
        }
    }

    class Stock
    {
        string symbol;
        double price;
        List<IInvestor> investors = new List<IInvestor>();

        public void Attach(IInvestor investor)
        {
            this.investors.Add(investor);
        }

        public void Detach(IInvestor investor)
        {
            this.investors.Remove(investor);
        }

        public Stock(string symbol, double price)
        {
            this.symbol = symbol;
            this.price = price;
        }

        public double GetPrice()
        {
            return this.price;
        }

        public void SetPrice(double price)
        {
            if (this.price != price)
            {
                this.price = price;
                Notify();
            }

        }

        private void Notify()
        {
            foreach (IInvestor investor in investors)
            {
                investor.Update(this);
            }

            // Ejemplo en multiples hilos       
            //Parallel
            //    .ForEach(investors, investor => investor.Update(this));
        }
    }

    interface IInvestor
    {
        void Update(Stock stock);
    }

    class Investor : IInvestor
    {
        private string name;
        private double price;
        public Investor(string name)
        {
            this.name = name;
        }
        public void Update(Stock stock)
        {
            this.price = stock.GetPrice();
            Console.WriteLine($"New price for {this.name} is {this.price}");
        }
    }
}
