﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalaDeChat
{
    class Program
    {
        static void Main(string[] args)
        {
            Participant Juan = new Participant("Juan");
            Participant Maria = new Participant("Maria");

            ChatRoom chatRoom = new ChatRoom();
            chatRoom.Register(Juan);
            chatRoom.Register(Maria);

            Juan.Send("Maria", "Hola! ¿Cómo estás?");
            Maria.Send("Juan", "Todo muy bien! Y vos!?");
        }
    }

    class ChatRoom
    {
        private Dictionary<string, Participant> participants = new Dictionary<string, Participant>();

        public void Register(Participant participant)
        {
            if (!participants.ContainsKey(participant.GetNickname()))
            {
                participants.Add(participant.GetNickname(), participant);
                participant.SetChatRoom(this);
            }
        }

        public void Send(string from, string to, string message)
        {
            Participant participant = participants[to];

            if (participant != null)
            {
                participant.Receive(from, message);
            }
        }
    }

    class Participant
    {
        private string nickname;
        private ChatRoom chatRoom;

        public Participant(string nickname)
        {
            this.nickname = nickname;
        }

        public void Send(string to, string message)
        {
            chatRoom.Send(this.nickname, to, message);
        }
        
        public void Receive(string from, string message)
        {
            Console.WriteLine($"From {from}: {message}");
        }

        public string GetNickname()
        {
            return this.nickname;
        }

        public void SetChatRoom(ChatRoom chatRoom)
        {
            this.chatRoom = chatRoom;
        }
    }


}
