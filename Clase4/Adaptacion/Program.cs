﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adaptacion
{
    class Program
    {
        static void Main(string[] args)
        {
        }
    }

    // Otro dominio
    class FacebookAPI
    {
        public void Post()
        {

        }
    }

    // Mi domio
    class Adapter
    {
        FacebookAPI facebookAPI;

        public void Post()
        {
            this.facebookAPI.Post();
        }
    }

    // Mi dominio
    class SocialNetwork
    {
        Adapter adapter;

        public void Post()
        {
            this.adapter.Post();
        }
    }
}
