﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    class Program
    {
        static void Main(string[] args)
        {
            Empleado gerente = new Gerente("Jorge");            
            Empleado vendedorPorComision = new VendedorPorComision("Carlos", 100);

            CalculadoraDeSalario calculadoraDeSalario = new CalculadoraDeSalario();

            Console.WriteLine("Gerente: " + calculadoraDeSalario.Obtener(gerente));
            Console.WriteLine("Vendedor Por Comision: " + calculadoraDeSalario.Obtener(vendedorPorComision));
        }
    }

    abstract class Empleado
    {
        private string nombre;        

        public Empleado(string nombre)
        {
            this.nombre = nombre;         
        }

        public abstract double ObtenerSalario();
    }

    class Gerente : Empleado
    {
        public Gerente(string nombre) : base(nombre)
        {
        }

        public override double ObtenerSalario()
        {
            return 100000;
        }
    }

    class Secretaria : Empleado
    {
        public Secretaria(string nombre) : base(nombre)
        {
        }

        public override double ObtenerSalario()
        {
            return 25000;
        }
    }

    class Supervisor : Empleado
    {
        public Supervisor(string nombre) : base(nombre)
        {
        }

        public override double ObtenerSalario()
        {
            return 70000;
        }
    }

    class Vendedor : Empleado
    {
        public Vendedor(string nombre) : base(nombre)
        {
        }

        public override double ObtenerSalario()
        {
            return 50000;
        }
    }

    class VendedorPorComision : Vendedor
    {
        private int cantidadDeVentas;
        private int porcentaje = 10;

        public VendedorPorComision(string nombre) : base(nombre)
        {
            
        }
    
        public VendedorPorComision(string nombre, int cantidadDeVentas) : this(nombre)
        {
            this.cantidadDeVentas = cantidadDeVentas;   
        }

        public override double ObtenerSalario()
        {
            return base.ObtenerSalario() + (cantidadDeVentas * porcentaje);
        }
    }

    class CalculadoraDeSalario
    {
        public double Obtener(Empleado empleado)
        {
            return empleado.ObtenerSalario();
        }
    }
}
