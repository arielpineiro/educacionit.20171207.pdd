﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaseLista
{
    class Program
    {
        static void Main(string[] args)
        {
            Lista lista = new Lista();
            lista.Agregar(1);
            lista.Agregar(11);
            lista.Agregar(21);
            lista.Agregar(3);

            lista.Ordenar(new QuickSort());


        }
    }

    class Lista
    {
        List<int> valores = new List<int>();

        public void Agregar(int valor)
        {
            this.valores.Add(valor);
        }

        public void Ordenar(MetodoDeOrdenamiento metodoDeOrdenamiento)
        {
            metodoDeOrdenamiento.Aplicar(valores);

            foreach (int valor in valores)
            {
                Console.WriteLine(valor);
            }
        }
        
    }

    abstract class MetodoDeOrdenamiento
    {
        public abstract void Aplicar(List<int> valores);
    }

    class BubbleSort : MetodoDeOrdenamiento
    {
        public override void Aplicar(List<int> valores)
        {
            //TODO: buscar de internet
        }
    }

    class QuickSort : MetodoDeOrdenamiento
    {
        public override void Aplicar(List<int> valores)
        {
            valores.Sort();
        }
    }
}
