﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BalanceadorDeCarga
{
    class Program
    {
        static void Main(string[] args)
        {
            LoadBalancer loadBalancer1 = LoadBalancer.GetInstance();
            LoadBalancer loadBalancer2 = LoadBalancer.GetInstance();
            
            if (loadBalancer1 != loadBalancer2)
                Console.WriteLine("Existen dos balanceadores de carga. ");
            else
                Console.WriteLine("Son la misma. ");

            Server server1 = loadBalancer1.GetServer();
            Server server2 = loadBalancer2.GetServer();

            Console.WriteLine(server1.GetAddress());
            Console.WriteLine(server2.GetAddress());
        }
    }

    class LoadBalancer
    {
        private List<Server> servers = new List<Server>();
        private static LoadBalancer instance;
        private static object padlock = new object();

        private LoadBalancer()
        {
            this.servers.Add(new Server("192.168.0.2"));
            this.servers.Add(new Server("192.168.0.3"));
            this.servers.Add(new Server("192.168.0.4"));
            this.servers.Add(new Server("192.168.0.5"));
            this.servers.Add(new Server("192.168.0.6"));
        }
        public Server GetServer()
        {
            return servers[new Random().Next(0, 4)];
        }
        
        public static LoadBalancer GetInstance()
        {
            if (instance == null)
            { 
                lock (padlock)
                {
                    if (instance == null)
                        instance = new LoadBalancer();
                }
            }
            return instance;
        }
    }

    class Server
    {
        private string address;

        public Server(string address)
        {
            this.address = address;
        }

        public string GetAddress()
        {
            return this.address;
        }
    }
}
