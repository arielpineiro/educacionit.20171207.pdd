﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    class Program
    {
        static void Main(string[] args)
        {
            Empleado gerente = new Empleado("Jorge", "gerente");
            Empleado vendedorPorComision = new Empleado("Carlos", "vendedorPorComision");

            CalculadoraDeSalario calculadoraDeSalario = new CalculadoraDeSalario();

            Console.WriteLine("Gerente: " + calculadoraDeSalario.Obtener(gerente, 0));
            Console.WriteLine("Vendedor Por Comision: " + calculadoraDeSalario.Obtener(vendedorPorComision, 10));
        }
    }

    class Empleado
    {
        private string nombre;
        private string tipo;

        public Empleado(string nombre, string tipo)
        {
            this.nombre = nombre;
            this.tipo = tipo;
        }

        public string Tipo()
        {
            return this.tipo;
        }
    }

    class CalculadoraDeSalario
    {
        public double Obtener(Empleado empleado, int cantidadDeVentas)
        {
            string tipo = empleado.Tipo();

            switch (tipo)
            {
                case "gerente": return 100000;
                case "secretaria": return 25000;
                case "supervisor": return 70000;
                case "vendedor": return 50000;
                case "vendedorPorComision": return 5000 + (cantidadDeVentas * 10);
                default: return 0;
            }
        }
    }
}
