﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animales
{
    class Program
    {
        static void Main(string[] args)
        {
            ContinentCreator africa = new AfricaCreator();

            AnimalWorld animalWorld = new AnimalWorld(africa);

            animalWorld.RunFood();
        }
    }

    class AnimalWorld
    {
        private Herbivore herbivore;
        private Carnivore carnivore;

        public AnimalWorld(ContinentCreator continentCreator)
        {
            this.herbivore = continentCreator.CreateHerbivore();
            this.carnivore = continentCreator.CreateCarnivore();
        }

        public void RunFood()
        {
            carnivore.Eat(herbivore);
        }
    }

    abstract class ContinentCreator
    {
        public abstract Herbivore CreateHerbivore();
        public abstract Carnivore CreateCarnivore();
    }

    class AfricaCreator : ContinentCreator
    {
        public override Carnivore CreateCarnivore()
        {
            return new Lion();
        }

        public override Herbivore CreateHerbivore()
        {
            return new Giraffe();
        }
    }

    class AmericaCreator : ContinentCreator
    {
        public override Carnivore CreateCarnivore()
        {
            throw new NotImplementedException();
        }

        public override Herbivore CreateHerbivore()
        {
            throw new NotImplementedException();
        }
    }

    abstract class Animal
    {

    }

    abstract class Carnivore : Animal
    {
        public abstract void Eat(Herbivore herbivore);
    }

    class Herbivore : Animal
    {

    }

    class Lion : Carnivore
    {
        public override void Eat(Herbivore herbivore)
        {
            Console.WriteLine("Mmm.... que rico!");
        }
    }

    class Giraffe : Herbivore
    {

    }
}
