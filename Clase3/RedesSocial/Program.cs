﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedesSocial
{
    class Program
    {
        static void Main(string[] args)
        {
            Platform ios = new iOS();
            SocialNetwork instagram = new Instagram(ios);
            instagram.Post();
        }
    }

    abstract class SocialNetwork
    {
        Platform platform;

        public SocialNetwork(Platform platform)
        {
            this.platform = platform;
        }

        public abstract void Post();
    }

    class Facebook : SocialNetwork
    {
        public Facebook(Platform platform) : base(platform)
        {
        }

        public override void Post()
        {
            throw new NotImplementedException();
        }
    }

    class Instagram : SocialNetwork
    {
        public Instagram(Platform platform) : base(platform)
        {
        }

        public override void Post()
        {
            throw new NotImplementedException();
        }
    }

    abstract class Platform
    {

    }

    class Android : Platform
    {

    }

    class iOS : Platform
    {

    }
}
