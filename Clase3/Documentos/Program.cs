﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Documentos
{
    class Program
    {
        static void Main(string[] args)
        {
            Document document = new Resume();

            foreach (Page page in document.GetPages())
            {
                Console.WriteLine("Página: " + page.GetType().Name);
            }
        }
    }

    abstract class Document
    {
        protected List<Page> pages = new List<Page>();

        public Document()
        {
            this.CreatePages();
        }

        public List<Page> GetPages()
        {
            return this.pages;
        }

        public abstract void CreatePages();
    }

    class Resume : Document
    {
        public override void CreatePages()
        {
            this.pages.Add(new SkillsPage());
            this.pages.Add(new EducationPage());
            this.pages.Add(new ExperiencePage());
        }
    }

    class Report : Document
    {
        public override void CreatePages()
        {
            throw new NotImplementedException();
        }
    }

    abstract class Page
    {

    }

    class SkillsPage : Page
    {

    }

    class EducationPage : Page
    {

    }

    class SummaryPage : Page
    {

    }

    class IntroductionPage : Page
    {

    }

    class ExperiencePage : Page
    {

    }
}
