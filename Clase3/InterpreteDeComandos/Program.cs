﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterpreteDeComandos
{
    class Program
    {
        static void Main(string[] args)
        {
            IShell shell = new MsDos();
            ITask backupTask = new BackupTask(shell);            
            ITask restoreTask = new RestoreTask(shell);

            List<ITask> tasks = new List<ITask>
            {
                backupTask,
                restoreTask
            };

            ITask executor = new Executor(tasks);
            executor.Execute();
        }
    }

    interface ITask
    {
        void Execute();
    }

    class Executor : ITask
    {
        private List<ITask> tasks;

        public Executor(List<ITask> tasks)
        {
            this.tasks = tasks;
        }
        public void Execute()
        {
            foreach (ITask task in tasks)
            {
                task.Execute();
            }
        }
    }

    class BackupTask : ITask
    {
        private IShell shell;

        public BackupTask(IShell shell)
        {
            this.shell = shell;
        }
        public void Execute()
        {
            // Crear tarea de Backup
            shell.Create();
            shell.Copy();
        }
    }

    class RestoreTask : ITask
    {
        private IShell shell;

        public RestoreTask(IShell shell)
        {
            this.shell = shell;
        }
        public void Execute()
        {
            // Crear una tarea de Restore
            shell.Copy();
            shell.Delete();
        }
    }

    interface IShell
    {
        void Copy();
        void Move();
        void Delete();
        void Create();
    }

    class MsDos : IShell
    {
        public void Copy()
        {
            Console.WriteLine("Copying...");
        }

        public void Create()
        {
            Console.WriteLine("Creating...");
        }

        public void Delete()
        {
            Console.WriteLine("Deleting...");
        }

        public void Move()
        {
            Console.WriteLine("Moveing...");
        }
    }

    class Bash : IShell
    {
        public void Copy()
        {
            throw new NotImplementedException();
        }

        public void Create()
        {
            throw new NotImplementedException();
        }

        public void Delete()
        {
            throw new NotImplementedException();
        }

        public void Move()
        {
            throw new NotImplementedException();
        }
    }
}
