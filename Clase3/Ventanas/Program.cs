﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ventanas
{
    //Java
    //  @Borde
    //  @Scroll
    //  @Tooltip
    //C#
    //  [Borde()]
    //  [Scroll()]
    //  [Tooltip()]    
    class Ventana
    {
        //[WebMethod()]
        public void Foo()
        {

        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            VisualComponent visualComponent = new Flavour(new Border(new Scroll(new Tooltip(new Window()))));
            visualComponent.Draw();
        }
    }

    abstract class VisualComponent
    {
        public abstract void Draw();
    }

    class Flavour : VisualComponent
    {
        VisualComponent visualComponent;
        public Flavour(VisualComponent visualComponent)
        {
            this.visualComponent = visualComponent;
        }

        public override void Draw()
        {         
            this.visualComponent.Draw();
        }
    }

    class Border : Flavour
    {
        public Border(VisualComponent visualComponent) : base(visualComponent)
        {
        }

        public override void Draw()
        {
            base.Draw();
            Console.WriteLine(this.GetType().Name);
        }
    }

    class Scroll : Flavour
    {
        public Scroll(VisualComponent visualComponent) : base(visualComponent)
        {
        }

        public override void Draw()
        {
            base.Draw();
            Console.WriteLine(this.GetType().Name);
        }
    }

    class Tooltip : Flavour
    {
        public Tooltip(VisualComponent visualComponent) : base(visualComponent)
        {
        }

        public override void Draw()
        {
            base.Draw();
            Console.WriteLine(this.GetType().Name);
        }
    }

    class Window : VisualComponent
    {
        public override void Draw()
        {            
            Console.WriteLine(this.GetType().Name);
        }
    }
}
